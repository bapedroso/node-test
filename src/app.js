'use strict';

var express = require('express');
var posts = require("./jsons/posts.json");

var app = express();

app.get('/', function(req, res){
  res.send("<h1>Bruno Pedroso, it is OK</h1>");
});

app.get('/blog', function(req, res){
  res.send(posts);
});

app.listen(3000, function(){
  console.log("The front end is running on 3000 port.");
});
